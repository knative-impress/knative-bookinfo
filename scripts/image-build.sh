#!/bin/bash
####################################################################
## [Knative Practice Guide]
## Build Container image by using Tekton Pipeline
## 
## Ver 1.0 Yudai Ono 
####################################################################

. setup.rc
LIST_FILE="image-builds.list"

####################################################################
## Delete the pipeline once the image build is complete to conserve resources
function chk_build_status(){
    count=0
    while true
    do
        status=$(kubectl get pipelinerun $1 -n bookinfo-builds -o jsonpath='{.status.conditions[*].reason}')
        if [ $status == "Succeeded" ];
        then
            echo -e "\\nbuild success: $1"
            echo ""
            kubectl delete pipelinerun $1 -n bookinfo-builds
            break
        else
            if [ $count -eq 0 ];
            then
                echo -n "wait for image build: $1"
            else
                echo -n "."
            fi
            count=`expr $count + 1`
            continue
        fi
    done
}

####################################################################
## Get the environment variable values listed in image-builds.list and run the pipeline
function run_pipleinerun(){
    for line in $(cat $LIST_FILE | grep -v "#")
    do 
        echo ""
        export SERVICE_NAME=$(echo $line | awk -F, '{print $1}')
        export IMAGE_NAME=$(echo $line | awk -F, '{print $2}')
        export IMAGE_REVISION=$(echo $line | awk -F, '{print $3}')
        export COLOR_ENABLE=$(echo $line | awk -F, '{print $4}')
        export STAR_COLOR=$(echo $line | awk -F, '{print $5}')

        if [ $IMAGE_NAME == "reviews" ];
        then
            echo "build: build-image-reviews-$IMAGE_REVISION"
            echo "---"
            cat $TEKTON_MANIFEST/pipelinerun-reviews.yaml | envsubst | kubectl create -f -
            chk_build_status "build-image-reviews-$IMAGE_REVISION"
        else
            echo "build: build-image-$IMAGE_NAME"
            echo "---"
            cat $TEKTON_MANIFEST/pipelinerun.yaml | envsubst | kubectl create -f -
            chk_build_status "build-image-$IMAGE_NAME"
        fi
    done
}

####################################################################
## main
run_pipleinerun
