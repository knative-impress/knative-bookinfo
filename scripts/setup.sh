#!/bin/bash
####################################################################
## [Knative Practice Guide]
## Building Practice Environment Script
## 
## Ver 1.0 Yudai Ono 
####################################################################

####################################################################
## Display usage
function usage {
  cat <<EOM
Usage: $(basename "$0") [OPTION]...
  -a    Run all steps
  -s    Run all steps except the image build step 
  -k    Run only EKS cluster installation step
  -p    Run the step after the installation of the EKS cluster
  -b    Deploy only Bookinfo microservices
  -c    Check all deployments status
  ---
  -h    Display help
EOM
  exit 2
}

####################################################################
## Import environment values
function import_config(){
 . setup.rc
}

####################################################################
## Check pods status in the specified namespace
function chk_pod_status(){
    count=0
    while true
    do
        POD_STATUS=$(kubectl get pods -n $1 2>&1 )
        NOT_RUNNING=$(echo -e "\\n$POD_STATUS" | egrep -v "NAME|Running|Completed" | wc -l)
        if [ $NOT_RUNNING -eq 1 ];
        then
            echo "Namespace: $1, Pod is Running!!"
            echo -e "\\n$POD_STATUS"
            break
        else
            if [ $count -eq 0 ];
            then
                echo -n "wait for Running"
            else
                echo -n "."
            fi
            sleep 3
            count=`expr $count + 1`
            continue
        fi
    done
}

####################################################################
# Setting AWS CLI
function setting_aws(){
    echo "---"
    echo "[START] Setting AWS CLI"

    cat <<EOF > ~/.aws/credentials
[default]
aws_access_key_id = ${IAM_ACCESS_KEY}
aws_secret_access_key = ${IAM_SECRET_ACCESS_KEY}
EOF

    cat <<EOF > ~/.aws/config
[default]
region = ${REGION}
output = ${AWS_CLI_OUTPUT}
EOF

    echo "[DONE] Setting AWS CLI"
    echo ""
}


####################################################################
## Create EKS Cluster
function create_k8s(){
    echo "---"
    echo "[START] Create EKS Cluster"

    ### Create EKS Cluster
    eksctl create cluster --name ${CLUSTER_NAME} \
    --version ${CLUSTER_VERSION} \
    --managed --nodegroup-name ${NODE_GROUP_NAME} \
    --node-type ${NODE_TYPE} --nodes ${NODES} --nodes-min 3 --nodes-max 10

    ### Configure kubeconfig
    aws eks update-kubeconfig --region ap-northeast-1 --name ${CLUSTER_NAME}

    ### Install ESB CSI Driver
    aws eks describe-cluster --name ${CLUSTER_NAME} --query "cluster.identity.oidc.issuer" --output text
    eksctl utils associate-iam-oidc-provider --cluster ${CLUSTER_NAME} --approve
    eksctl create iamserviceaccount \
    --name ebs-csi-controller-sa \
    --namespace kube-system \
    --cluster ${CLUSTER_NAME} \
    --attach-policy-arn arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy \
    --approve \
    --role-only \
    --role-name AmazonEKS_EBS_CSI_DriverRole
    eksctl create addon --name aws-ebs-csi-driver \
    --cluster ${CLUSTER_NAME} \
    --service-account-role-arn arn:aws:iam::${AWS_ACCOUNT_ID}:role/AmazonEKS_EBS_CSI_DriverRole \
    --force

    ### wait for running the pod
    chk_pod_status kube-system

    echo "[DONE] Create EKS Cluster"
    echo ""
}

####################################################################
## Create namespace
function create_namespace(){
    kubectl create ns tekton-pipelines
    kubectl apply -f ${MANIFEST_PATH}/tekton/namespace.yaml 
    kubectl apply -f ${MANIFEST_PATH}/serving/namespace.yaml
    kubectl apply -f ${MANIFEST_PATH}/k8s-deployment/kafka/namespace.yaml
}

####################################################################
## Install Knative
function install_knative(){
    echo "---"
    echo "[START] Install Knative"

    ### Install Knative Operator
    kn operator install -n knative-operator -v ${KNATIVE_VERSION}
    ### wait for running the pod
    chk_pod_status knative-operator

    ### Install Knative Serving
    kn operator install --component serving -n knative-serving -v ${KNATIVE_VERSION} --kourier

    ### wait for running the pod
    chk_pod_status knative-serving

    ### Configure Knative Ingress
    kn operator enable ingress --kourier -n knative-serving

    ### Install Knative Eventing
    kn operator install --component eventing -n knative-eventing -v ${KNATIVE_VERSION}

    ### wait for running the pod
    chk_pod_status knative-eventing

    echo "[DONE] Install Knative"
    echo ""
}

####################################################################
## Install Tekton
function install_tekton(){
    echo "---"
    echo "[START] Install Tekton"

    kubectl config set-context $(kubectl config current-context) --namespace=tekton-pipelines
    kubectl apply -f https://storage.googleapis.com/tekton-releases/operator/latest/release.yaml
    chk_pod_status tekton-operator
    kubectl apply -f ${MANIFEST_PATH}/tekton/tektonconfig.yaml

    ### wait for running the pod
    chk_pod_status tekton-pipelines
    echo "...wait for 30s..."
    sleep 30

    ### Deploy Tekton Pipeline for building container images
    kubectl apply -f ${MANIFEST_PATH}/common/sa-knative-admin.yaml
    cat ${MANIFEST_PATH}/common/gitlab-token.yaml | envsubst | kubectl apply -f -

    ## Install Tekton Task from Tetktonhub
    tkn hub install task git-clone -n bookinfo-builds --version 0.9
    tkn hub install task kaniko -n bookinfo-builds --version 0.6
    tkn hub install task gradle -n bookinfo-builds --version 0.2

    ## Tekton Pipelineの構築
    kubectl create -f ${MANIFEST_PATH}/tekton/pipeline

    echo "[DONE] Install Tekton"
    echo ""
}

####################################################################
## Build container image for bookinfo applications, and push the images to GitLab.com
function image_build(){
    echo "---"
    echo "[START] Build container image for bookinfo applications, and push the images to GitLab.com"

    ./image-build.sh

    echo "[DONE] Build container image for bookinfo applications, and push the images to GitLab.com"
    echo ""
}

####################################################################
## Configure Knative Serving
function serving_configuration(){
    echo "---"
    echo "[START] Configure Knative Serving"

    ## Change Knative Serving Default Dmain
    export KNATIVE_VERSION=$(kubectl get knativeserving -n knative-serving -o jsonpath={.items[*].status.version})
    kubectl apply -f https://github.com/knative/serving/releases/download/knative-v${KNATIVE_VERSION}/serving-default-domain.yaml

    ## Install Let's Encrypt
    kubectl config set-context $(kubectl config current-context) --namespace=bookinfo
    kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/${CERT_VERSION}/cert-manager.yaml

    ### wait for running the pod
    chk_pod_status cert-manager
    echo "...wait for 60s..."
    sleep 60

    ## apply clusterissuer
    kubectl apply -f ${MANIFEST_PATH}/common/clusterissuer.yaml
    export KNATIVE_VERSION=$(kubectl get knativeserving -n knative-serving -o jsonpath={.items[*].status.version})

    ## install net-certmanager
    kubectl apply -f https://github.com/knative/net-certmanager/releases/download/knative-v${KNATIVE_VERSION}/release.yaml
    kubectl patch configmap/config-certmanager \
    -n knative-serving \
    --type merge \
    -p '{"data": {"issuerRef": "kind: ClusterIssuer\nname: letsencrypt-http01-issuer\n"}}'

    ## patch knative serving config
    kubectl patch knativeserving/knative-serving \
    -n knative-serving \
    --type merge \
    -p '{"spec": {"config": { "network": { "auto-tls": "Enabled", "autocreate-cluster-domain-claims": "true", "http-protocol": "Enabled"}}}}'

    echo "[DONE] Configure Knative Serving"
    echo ""
}

####################################################################
## Deploy MySQL
function deploy_mysql(){
    echo "---"
    echo "[START] Deploy MySQL"

    kubectl config set-context $(kubectl config current-context) --namespace=bookinfo
    kubectl apply -f ${MANIFEST_PATH}/k8s-deployment/mysql

    ### wait for running the pod
    chk_pod_status bookinfo

    echo "[DONE] Deploy MySQL"
    echo ""
}
####################################################################
## Deploy Kafka
function deploy_kafka(){
    echo "---"
    echo "[START] Deploy Kafka"

    ## Install Strimzi
    kubectl config set-context $(kubectl config current-context) --namespace=kafka
    kubectl create -f 'https://strimzi.io/install/latest?namespace=kafka' -n kafka

    ### wait for running the pod
    chk_pod_status kafka

    ## Deploy Strimzi
    kubectl apply -f ${MANIFEST_PATH}/k8s-deployment/kafka/kafka.yaml

    ### wait for running the pod
    chk_pod_status kafka
    echo "...wait for 60s..."
    sleep 60

    ## Install KafDrop
    kubectl config set-context $(kubectl config current-context) --namespace=kafka
    helm repo add rhcharts https://ricardo-aires.github.io/helm-charts/
    helm upgrade --install aires \
    --set kafka.enabled=false \
    --set kafka.bootstrapServers=my-cluster-kafka-bootstrap.kafka.svc.cluster.local:9092 rhcharts/kafdrop

    echo "[DONE] Deploy Kafka"
    echo ""
}

####################################################################
#＃ Deploy Bookinfo apps
function deploy_bookinfo_apps(){
    echo "---"
    echo "[START] Deploy Bookinfo apps"

    kubectl config set-context $(kubectl config current-context) --namespace=bookinfo

    ## create secret
    kubectl create secret docker-registry registry-token -n bookinfo \
    --docker-server=registry.gitlab.com \
    --docker-username=${GITLAB_USER} \
    --docker-password=${GITLAB_TOKEN}

    ## create service account
    kubectl apply -f ${MANIFEST_PATH}/common/sa-knative-deployer.yaml

    ## deploy applications
    cat ${MANIFEST_PATH}/serving/bookinfo/* | envsubst | kubectl apply -f -

    ## update productpage 
    kn service update productpage --namespace bookinfo \
    --image registry.gitlab.com/${GITLAB_USER}/knative-bookinfo/productpage:v2 \
    --env ORDERS_HOSTNAME=order.bookinfo.svc.cluster.local

    ## deploy bookorder
    cat ${MANIFEST_PATH}/serving/bookorder/* | envsubst | kubectl apply -f -

    echo "[DONE] Deploy Bookinfo apps"
    echo ""
}


####################################################################
## Deploy Eventing-Kafka
function deploy_eventing_kafka(){
    echo "---"
    echo "[START] Deploy Eventing-Kafka"

    kubectl apply -f \
    https://github.com/knative-sandbox/eventing-kafka-broker/releases/download/knative-${KNATIVE_KAFKA_SOURCE_VERSION}/eventing-kafka-controller.yaml
    kubectl apply -f \
    https://github.com/knative-sandbox/eventing-kafka-broker/releases/download/knative-${KNATIVE_KAFKA_SOURCE_VERSION}/eventing-kafka-source.yaml
    kubectl apply -f \
    https://github.com/knative-sandbox/eventing-kafka-broker/releases/download/knative-${KNATIVE_KAFKA_SOURCE_VERSION}/eventing-kafka-channel.yaml
    kubectl apply -f \
    https://github.com/knative-sandbox/eventing-kafka-broker/releases/download/knative-${KNATIVE_KAFKA_SOURCE_VERSION}/eventing-kafka-broker.yaml
    kubectl apply -f \
    https://github.com/knative-sandbox/eventing-kafka-broker/releases/download/knative-${KNATIVE_KAFKA_SOURCE_VERSION}/eventing-kafka-sink.yaml

    ## patch Kafka Broker Config
    kubectl apply -f ${MANIFEST_PATH}/common/kafka-broker-config.yaml

    echo "[DONE] Deploy Eventing-Kafka"
    echo ""
}

####################################################################
## Check Deployment Status
function check_all_deployments(){
    echo "---"
    echo "[CHECK] ALL"
    echo ""

    kubectl get nodes
    echo ""
    kubectl get pod -n tekton-pipelines
    echo ""
    tkn task list -n bookinfo-builds
    echo ""
    tkn pipeline list -n bookinfo-builds
    echo ""
    kubectl get deployment -n knative-serving
    echo ""
    kubectl get deployment -n knative-eventing
    echo ""
    kubectl get pod -n kafka
    echo ""
    kn service list -n bookinfo
    echo ""
    kubectl config set-context $(kubectl config current-context) --namespace=bookinfo

    echo "---"
    echo "[DONE] ALL"
    echo ""
}

####################################################################
## install all
function all_step(){
    import_config
    setting_aws
    create_k8s
    create_namespace
    install_knative
    install_tekton
    image_build
    serving_configuration
    deploy_kafka
    deploy_eventing_kafka
    deploy_mysql
    deploy_bookinfo_apps
    check_all_deployments
}

####################################################################
## Skip an image build step
function skip_image_build(){
    import_config
    setting_aws
    create_k8s
    create_namespace
    install_knative
    install_tekton
    serving_configuration
    deploy_kafka
    deploy_eventing_kafka
    deploy_mysql
    deploy_bookinfo_apps
    check_all_deployments
}

####################################################################
## Deploy only k8s cluster step
function deploy_only_k8s(){
    import_config
    setting_aws
    create_k8s
}

####################################################################
## Deploy bookinfo components
function deploy_components(){
    import_config
    create_namespace
    install_knative
    install_tekton
    serving_configuration
    deploy_kafka
    deploy_eventing_kafka
    deploy_mysql
    deploy_bookinfo_apps
    check_all_deployments
}

####################################################################
## Deploy bookinfo microservies
function deploy_bookinfo_microservices(){
    import_config
    create_namespace
    deploy_bookinfo_apps
    check_all_deployments
}

####################################################################
## main
if [ $# = 0 ]; then
    usage
    exit 1
else
    while getopts abckpsh optKey; do
        case "$optKey" in
            a) all_step ;;
            b) deploy_bookinfo_microservices ;;
            c) check_all_deployments ;;
            k) deploy_only_k8s ;;
            p) deploy_components ;;
            s) skip_image_build ;;
            h|*) usage ;;
        esac
    done
fi