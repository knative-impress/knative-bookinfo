![title](./img/title.png)

本リポジトリは、インプレス社出版「[Knative 実践ガイド](https://book.impress.co.jp/books/1122101070)」の演習用リポジトリです。

## 0. 更新履歴
- [2023年3月27日] 最新の[v0.30.0](https://github.com/tektoncd/cli/releases)を使用してください。
- [2023年5月11日] ratingsサービスのコンテナのベースイメージを変更しました。

## 1. 環境要件
### 1-1. CLIツール
- [aws](https://docs.aws.amazon.com/ja_jp/cli/latest/userguide/getting-started-install.html)
- [eksctl](https://docs.aws.amazon.com/ja_jp/eks/latest/userguide/eksctl.html)
- [kubectl](https://kubernetes.io/docs/tasks/tools/)
- [helm](https://helm.sh/ja/docs/intro/install/)
- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [envsubst](https://github.com/a8m/envsubst)
- [tkn](https://tekton.dev/docs/cli)
  - [2023年3月27日] 最新の[v0.30.0](https://github.com/tektoncd/cli/releases)を使用してください。
- [kn](https://knative.dev/docs/client/install-kn)

### 1-2. テスト済みの環境
#### クラウドリソース
- クラウド環境: Amazon Web Service（AWS）
- Amazon Elastic Kubernetes Service（EKS）
  - Kubernetes: v1.24
  - マシンタイプ: t3.medium （仮想 vCPU: 2 コア、仮想メモリ: 4GB）
  - サーバ台数: 6 台 (オートスケールの演習をしない場合は、最低でも5台必要です)
- GitLab (コンテナレジストリもGitLabを使用する前提となっています)

#### Kubernetes上のソフトウェア
- Knative: v1.9.0
- Tekton Operator: v0.64.0
- Tekton Pipeline: v0.42.0
- Kafka: v3.3.1
- MySQL: v5.7
- Cert Manager: v1.11.0

## 2.リポジトリ構成
```
$ tree ./knative-bookinfo
.
├── manifest ... マニフェスト格納ディレクトリ
│   ├── common ... SecretやService Accountなどのマニフェスト
│   ├── eventing ...Knative Eventing演習用のマニフェスト 
│   ├── k8s-deployment ...演習で使用するKubernetes Deploymentのマニフェスト 
│   ├── serving　...Knative Serving演習用のマニフェスト 
│   └── tekton ...Tekton関連のマニフェスト 
└── scripts ...2章の環境構築自動化スクリプト(AWSへAdministratorAccessが必要)
└── src ...ソースコード格納ディレクトリ 
    ├── productpage ...BookinfoのProductpage 
    ├── details ...BookinfoのDetails 
    ├── ratings ...BookinfoのRatings 
    ├── reviews ...BookinfoのReviews 
    ├── productpage-v2 ...第4章で使用するProductpageの更新版 
    ├── order ...BookorderのOrder 
    ├── stock ...BookorderのStock 
    ├── delivery ...BookorderのDelivery 
    ├── cloudeventer ...第4章で追加する試験用アプリケーション 
    └── stock-watcher ...第4章で追加する試験用アプリケーション 
```

## 3. Getting started

手動で環境構築する方法は「[Knative 実践ガイド](https://book.impress.co.jp/books/1122101070)」の第2章を参照してください。
(Bookorderのデプロイは一部第4章に記載しています)

ここでは、`setup.sh` を使用した自動構築の方法と簡単な動作確認方法を記載します。

### 3-1. AWSとGitLabのアカウント情報を指定

書籍では権限を限定したIAMユーザを作成していますが、ここでは管理者アクセス可能なIAMユーザを使用します。

なお、Gitリポジトリ、およびコンテナレジストリはいずれもGitLabを使用します。

```
$ vi scripts/setup.rc
# AWS Information
## AdministratorAccessが許可されるIAMのアクセスキーとシークレットアクセスキーを使用してください
export IAM_ACCESS_KEY="<Your IAM Access Key>"
export IAM_SECRET_ACCESS_KEY="<Your Secret Access Key>"
export AWS_ACCOUNT_ID="<Your AWS Account ID>"
...
# GitLab Information
export GITLAB_USER="<Your GitLab Username>"
export GITLAB_TOKEN="<Your GitLab Personal Access Token>"
...
```
### 3-2. スクリプトの実行
`setup.sh`を実行し環境構築します。このスクリプトを実行すると、AWS上に指定した台数のWorkerノード(default: 5台)を含むKubernetesクラスタが自動で構築されます。

※完了までの所要時間は、約 50 分

```
$ cd scripts
$ ./setup.sh 
Usage: setup.sh [OPTION]...
  -a    Run all steps ... # 全てのステップを実行します
  -s    Run all steps except the image build step ... # イメージビルド以外のステップを実行します
  -k    Run only EKS cluster installation step ... # EKSクラスタの構築のみ行います 
  -p    Run the step after the installation of the EKS cluster ... # EKSクラスタ構築後のステップを実行します
  -b    Deploy only Bookinfo microservices ... # Bookinfoの各マイクロサービスをデプロイします
  -c    Check all deployments status ... # デプロイしたコンポーネントの状態を確認します
  ---
  -h    Display help

$ ./setup.sh -a
```

Workerノードが足りずPodが起動しない場合、以下のコマンドを実行してWorkerノードを追加してください。

```
$ . setup.rc
$ eksctl scale nodegroup --cluster=${CLUSTER_NAME} --nodes=<ワーカーノードの台数> --name=${NODE_GROUP_NAME}
```

また、Kubernetesクラスタデプロイ後の `kubectl apply`の作業に失敗する場合は、以下のコマンドを再実行してください。

```
$ ./setup.sh -p
```

### 3-3. アクセス確認
`setup.sh`を実行後、最終的に以下の結果が出力されれば正常にデプロイできています。

```
$ ./setup.sh -c
NAME                                                STATUS   ROLES    VERSION
ip-192-168-12-43.ap-northeast-1.compute.internal    Ready    <none>   v1.24.10-eks-48e63af
ip-192-168-28-16.ap-northeast-1.compute.internal    Ready    <none>   v1.24.10-eks-48e63af
ip-192-168-50-38.ap-northeast-1.compute.internal    Ready    <none>   v1.24.10-eks-48e63af
ip-192-168-78-174.ap-northeast-1.compute.internal   Ready    <none>   v1.24.10-eks-48e63af

NAME                                                 READY   STATUS    RESTARTS
tekton-operator-proxy-webhook-5474bfdb6-mfx4c        1/1     Running   0       
tekton-pipelines-controller-785c7c8cbc-hcznw         1/1     Running   0       
tekton-pipelines-remote-resolvers-84c9cf6bbf-h5jwj   1/1     Running   0       
tekton-pipelines-webhook-957c658b4-npqqk             1/1     Running   0       

NAME        DESCRIPTION              AGE
git-clone   These Tasks are Git...   37 minutes ago
gradle      This Task can be us...   37 minutes ago
kaniko      This Task builds a ...   37 minutes ago

NAME                               LAST RUN   STARTED   DURATION   STATUS
pipelines-bookinfo-build           ---        ---       ---        ---
pipelines-bookinfo-build-reviews   ---        ---       ---        ---

NAME                         READY   UP-TO-DATE   AVAILABLE
3scale-kourier-gateway       1/1     1            1        
activator                    1/1     1            1        
autoscaler                   1/1     1            1        
autoscaler-hpa               1/1     1            1        
controller                   1/1     1            1        
domain-mapping               1/1     1            1        
domainmapping-webhook        1/1     1            1        
net-certmanager-controller   1/1     1            1        
net-certmanager-webhook      1/1     1            1        
net-kourier-controller       1/1     1            1        
webhook                      1/1     1            1        

NAME                       READY   UP-TO-DATE   AVAILABLE
eventing-controller        1/1     1            1        
eventing-webhook           1/1     1            1        
imc-controller             1/1     1            1        
imc-dispatcher             1/1     1            1        
kafka-broker-dispatcher    1/1     1            1        
kafka-broker-receiver      1/1     1            1        
kafka-channel-dispatcher   1/1     1            1        
kafka-channel-receiver     1/1     1            1        
kafka-controller           1/1     1            1        
kafka-sink-receiver        1/1     1            1        
kafka-webhook-eventing     1/1     1            1        
mt-broker-controller       1/1     1            1        
mt-broker-filter           1/1     1            1        
mt-broker-ingress          1/1     1            1        
pingsource-mt-adapter      0/0     0            0        

NAME                                          READY   STATUS    RESTARTS
aires-kafdrop-8d8bcbb55-8qqgd                 1/1     Running   0       
my-cluster-entity-operator-5677ddf87c-rt8f2   3/3     Running   0       
my-cluster-kafka-0                            1/1     Running   0       
my-cluster-zookeeper-0                        1/1     Running   0       
strimzi-cluster-operator-58967f75c7-dmzp5     1/1     Running   0       

NAME            URL                                                  LATEST                 CONDITIONS   READY   
delivery        http://delivery.bookinfo.svc.cluster.local           delivery-00001         3 OK / 3     True    
details         http://details.bookinfo.svc.cluster.local            details-00001          3 OK / 3     True    
order           http://order.bookinfo.svc.cluster.local              order-00001            3 OK / 3     True    
productpage     https://productpage.bookinfo.<IPアドレス>.sslip.io    productpage-00002      3 OK / 3     True    
ratings         http://ratings.bookinfo.svc.cluster.local            ratings-00001          3 OK / 3     True    
reviews         http://reviews.bookinfo.svc.cluster.local            reviews-00001          3 OK / 3     True    
stock           http://stock.bookinfo.svc.cluster.local              stock-00001            3 OK / 3     True    
stock-watcher   http://stock-watcher.bookinfo.svc.cluster.local      stock-watcher-00001    3 OK / 3     True 
```

ブラウザ(Google Chrome推奨)を開いて以下のURLへアクセスし、BookinfoのGUIが表示されることを確認してください。

```
https://productpage.bookinfo.<IPアドレス>.sslip.io/productpage
```

![bookinfo](./img/bookinfo.png)


## 4. おまけ
### 4-1. KafdropのGUIへアクセス

`kubectl proxy`でローカルへポートフォワードした上で、以下のURLへアクセスします。

```
$ kubectl proxy
```

```
http://localhost:8001/api/v1/namespaces/kafka/services/aires-kafdrop:9000/proxy/
```

![kafdrop](./img/kafdrop.png)

### 4-2. Tekton Dashboardのデプロイ＆アクセス

```
$ kubectl apply -f ./manifest/tekton/tektondashboard.yaml
tektondashboard.operator.tekton.dev/dashboard created

$ kubectl get pods -n tekton-pipelines
NAME                                                 READY   STATUS    RESTARTS   AGE
tekton-dashboard-678ffdbddd-zbxjc                    1/1     Running   0          9s
tekton-operator-proxy-webhook-5474bfdb6-98ql6        1/1     Running   0          31m
tekton-pipelines-controller-785c7c8cbc-7ks6z         1/1     Running   0          31m
tekton-pipelines-remote-resolvers-84c9cf6bbf-qrf9x   1/1     Running   0          31m
tekton-pipelines-webhook-957c658b4-wfs2p             1/1     Running   0          31m
```

`kubectl proxy`でローカルへポートフォワードした上で、以下のURLへアクセスします。

```
$ kubectl proxy
```

```
http://localhost:8001/api/v1/namespaces/tekton-pipelines/services/tekton-dashboard:http/proxy/
```

![tektondashboard](./img/tektondashboard.png)

## 5. LICENSE
本リポジトリで公開される情報は「Apache 2.0」ライセンスに準じます。
