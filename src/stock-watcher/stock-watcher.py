import os
import uuid
from datetime import datetime
import logging
import json
from flask import Flask, request,make_response
import requests
from kafka import KafkaProducer

# instantiate the API
app = Flask(__name__)

# 環境変数を取得
KAFKA_TOPIC = "stock" if (os.environ.get("KAFKA_TOPIC") is None) else os.environ.get("KAFKA_TOPIC")
KAFKA_BOOTSTRAP_SERVERS = "localhost:9092" if (os.environ.get("KAFKA_BOOTSTRAP_SERVERS") is None) else os.environ.get("KAFKA_BOOTSTRAP_SERVERS")
SERVICE_DOMAIN = "" if (os.environ.get("SERVICES_DOMAIN") is None) else "." + os.environ.get("SERVICE_DOMAIN")
SERVICE_PORT = "9080" if (os.environ.get("SERVICE_PORT") is None) else os.environ.get("SERVICE_PORT")
STOCK_HOSTNAME = "stock" if (os.environ.get("STOCK_HOSTNAME") is None) else os.environ.get("STOCK_HOSTNAME")
PRODUCER_MODE = False if (os.environ.get("PRODUCER_MODE") is None) else os.getenv('PRODUCER_MODE', 'False').lower() == 'true'
TIMEOUT = "10.0" if (os.environ.get("TIMEOUT") is None) else os.environ.get("TIMEOUT")


# Loggerを初期化
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

stock = {
    "name": "http://{0}{1}:{2}".format(STOCK_HOSTNAME, SERVICE_DOMAIN, SERVICE_PORT),
    "endpoint": "stock",
    "children": []
}

# stock-count topicを生成する
def produce(_id: int, _product_id: int, _state: str, _user:str):
    # Kafka Prodcuer initialize
    producer = KafkaProducer(bootstrap_servers=[KAFKA_BOOTSTRAP_SERVERS])

    # トピックへ送信するデータを構成
    value = { 'created_at': datetime.now().strftime('%Y/%m/%d %H:%M:%S'), 'id': _id, 'product_id': _product_id, 'status': _state, 'user': _user }
    log.info(f'Sending message with value -> {value}')
    value_json = json.dumps(value).encode('utf-8')

    try:
        # トピック送信
        producer.send(KAFKA_TOPIC, value_json)
    finally:
        producer.close()

# StockへGET参照して在庫数を返却する
def count(_product_id: int) -> None:
    try:
        url = stock['name'] + "/" + stock['endpoint'] + "/" + str(_product_id)
        log.info(f"access url: {url}")
        headers = {'content-type': 'application/json'}
        res = requests.get(url, headers=headers, timeout=float(TIMEOUT))
    except BaseException:
        res = None
    if res and res.status_code == 200:
        data = res.json()
        log.info(f"response data: {data}")
        _state = f"STOCK NUMBER: {data['count']}"
        return 200, _state
    else:
        status_code = res.status_code if res is not None and res.status_code else 500
        return status_code, {'error': 'Sorry, Stock is currently unavailable for this book.'}

# クラウドイベントの受信
@app.route("/", methods=["POST"])
def receive_cloudevents():

    # クラウドイベントを受信する    
    log.info(f"received cloudevents data: {request.data}")

    event = json.loads( request.data )
    _order_id = int(event["id"])
    _product_id = int(event["product_id"])
    _state = str(event["status"])
    _user = str(event["user"])

    # 現在の在庫数を取得する
    status_code, _state = count(_product_id)

    if PRODUCER_MODE:
        # Kafka Topicへ直接Publish
        produce(_order_id, _product_id, _state, _user)
        # PRODUCER_MODEがTrueの場合、Kafka Topicを送信し、HTTPレスポンスとしてイベントデータは返さない
        return "", status_code
    else:
        # 新たなイベントとしてHTTPレスポンス
        event_time = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        value = { 'created_at': event_time, 'id': _order_id, 'product_id': _product_id, 'status': _state, 'user': _user }
        response = make_response( json.dumps(value).encode('utf-8') )
        response.headers["Ce-Id"] = str(uuid.uuid4())
        response.headers["Ce-Source"] = "dev.knative.serving#stock-watcher"
        response.headers["Ce-specversion"] = "1.0"
        response.headers["Ce-Type"] = "cloudevent.event.type"
    return response, status_code

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=9080)