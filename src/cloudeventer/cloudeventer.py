import os
import json
import logging
from datetime import datetime
from cloudevents.conversion import to_binary
from cloudevents.http import CloudEvent
import requests

# 環境変数
# SinkBindingがSubjectに指定したリソースへ挿入
K_SINK = "" if (os.environ.get("K_SINK") is None) else os.environ.get("K_SINK")
K_CE_OVERRIDES = "" if (os.environ.get("K_CE_OVERRIDES") is None) else os.environ.get("K_CE_OVERRIDES")

# cloudeventer固有の環境変数
ORDER_ID = 1 if (os.environ.get("ORDER_ID") is None) else os.environ.get("ORDER_ID")
PRODUCT_ID = 0 if (os.environ.get("PRODUCT_ID") is None) else os.environ.get("PRODUCT_ID")
MESSAGE = "this is test message" if (os.environ.get("MESSAGE") is None) else os.environ.get("MESSAGE")
USER = "testuser" if (os.environ.get("USER") is None) else os.environ.get("USER")
TIMEOUT = 3.0 if (os.environ.get("TIMEOUT") is None) else os.environ.get("TIMEOUT")

# Loggerの初期化
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

# CloudEventの送信先 K_SINK　が指定されている場合のみ処理を実行
if K_SINK:
    # CloudEventsのヘッダ情報を構成
    attributes = {
        "type": "cloudevent.event.type",
        "source": "dev.knative.serving#cloudeventer",
        }
    # K_CE_OVERRIDESが存在する場合はattributesに追加
    if K_CE_OVERRIDES:
        extensions = json.loads(K_CE_OVERRIDES)
        attributes.update(extensions)

    # CloudEventのペイロードデータの作成
    event_time = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
    data = { 'created_at': event_time, 'id': int(ORDER_ID), 'product_id': int(PRODUCT_ID), 'status': MESSAGE, 'user': USER}
    event = CloudEvent(attributes, data)
        
    # バイナリモードでヘッダとボディを構成
    headers, body = to_binary(event)
    
    log.info(f'Sending CloudEvent with value headers: {headers}, body:{body}')

    # CloudEventを　K_SINK　宛にHTTP POST
    requests.post(K_SINK, data=body, headers=headers, timeout=float(TIMEOUT))

else:
    print("K_SINK is NULL, so skip processing")